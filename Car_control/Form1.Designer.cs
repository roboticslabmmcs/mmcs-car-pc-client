﻿namespace Car_control
{
    partial class Car_control
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.send_line = new System.Windows.Forms.TextBox();
            this.throttle = new System.Windows.Forms.TrackBar();
            this.steering = new System.Windows.Forms.TrackBar();
            this.send_btn = new System.Windows.Forms.Button();
            this.run_btn = new System.Windows.Forms.Button();
            this.stop_btn = new System.Windows.Forms.Button();
            this.left_btn = new System.Windows.Forms.Button();
            this.right_btn = new System.Windows.Forms.Button();
            this.center_btn = new System.Windows.Forms.Button();
            this.log = new System.Windows.Forms.TextBox();
            this.CarPorts = new System.Windows.Forms.ComboBox();
            this.car_connect_btn = new System.Windows.Forms.Button();
            this.SensorsPorts = new System.Windows.Forms.ComboBox();
            this.sensors_connect_btn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.throttle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.steering)).BeginInit();
            this.SuspendLayout();
            // 
            // send_line
            // 
            this.send_line.Location = new System.Drawing.Point(13, 187);
            this.send_line.Name = "send_line";
            this.send_line.Size = new System.Drawing.Size(333, 20);
            this.send_line.TabIndex = 1;
            // 
            // throttle
            // 
            this.throttle.Location = new System.Drawing.Point(12, 338);
            this.throttle.Maximum = 100;
            this.throttle.Name = "throttle";
            this.throttle.Size = new System.Drawing.Size(432, 45);
            this.throttle.TabIndex = 2;
            this.throttle.Value = 50;
            this.throttle.ValueChanged += new System.EventHandler(this.throttle_ValueChanged);
            // 
            // steering
            // 
            this.steering.Location = new System.Drawing.Point(13, 287);
            this.steering.Maximum = 100;
            this.steering.Name = "steering";
            this.steering.Size = new System.Drawing.Size(431, 45);
            this.steering.TabIndex = 3;
            this.steering.Value = 50;
            this.steering.ValueChanged += new System.EventHandler(this.steering_ValueChanged);
            // 
            // send_btn
            // 
            this.send_btn.Location = new System.Drawing.Point(369, 179);
            this.send_btn.Name = "send_btn";
            this.send_btn.Size = new System.Drawing.Size(75, 35);
            this.send_btn.TabIndex = 4;
            this.send_btn.Text = "SEND";
            this.send_btn.UseVisualStyleBackColor = true;
            this.send_btn.Click += new System.EventHandler(this.send_btn_Click);
            // 
            // run_btn
            // 
            this.run_btn.Location = new System.Drawing.Point(13, 225);
            this.run_btn.Name = "run_btn";
            this.run_btn.Size = new System.Drawing.Size(76, 42);
            this.run_btn.TabIndex = 5;
            this.run_btn.Text = "RUN";
            this.run_btn.UseVisualStyleBackColor = true;
            this.run_btn.Click += new System.EventHandler(this.run_btn_Click);
            // 
            // stop_btn
            // 
            this.stop_btn.Location = new System.Drawing.Point(109, 225);
            this.stop_btn.Name = "stop_btn";
            this.stop_btn.Size = new System.Drawing.Size(75, 42);
            this.stop_btn.TabIndex = 6;
            this.stop_btn.Text = "STOP";
            this.stop_btn.UseVisualStyleBackColor = true;
            this.stop_btn.Click += new System.EventHandler(this.stop_btn_Click);
            // 
            // left_btn
            // 
            this.left_btn.Location = new System.Drawing.Point(190, 225);
            this.left_btn.Name = "left_btn";
            this.left_btn.Size = new System.Drawing.Size(75, 42);
            this.left_btn.TabIndex = 7;
            this.left_btn.Text = "LEFT";
            this.left_btn.UseVisualStyleBackColor = true;
            this.left_btn.Click += new System.EventHandler(this.left_btn_Click);
            // 
            // right_btn
            // 
            this.right_btn.Location = new System.Drawing.Point(271, 225);
            this.right_btn.Name = "right_btn";
            this.right_btn.Size = new System.Drawing.Size(75, 42);
            this.right_btn.TabIndex = 8;
            this.right_btn.Text = "RIGHT";
            this.right_btn.UseVisualStyleBackColor = true;
            this.right_btn.Click += new System.EventHandler(this.right_btn_Click);
            // 
            // center_btn
            // 
            this.center_btn.Location = new System.Drawing.Point(369, 225);
            this.center_btn.Name = "center_btn";
            this.center_btn.Size = new System.Drawing.Size(75, 42);
            this.center_btn.TabIndex = 9;
            this.center_btn.Text = "CENTER";
            this.center_btn.UseVisualStyleBackColor = true;
            this.center_btn.Click += new System.EventHandler(this.center_btn_Click);
            // 
            // log
            // 
            this.log.Location = new System.Drawing.Point(12, 12);
            this.log.Multiline = true;
            this.log.Name = "log";
            this.log.ReadOnly = true;
            this.log.Size = new System.Drawing.Size(324, 161);
            this.log.TabIndex = 10;
            // 
            // CarPorts
            // 
            this.CarPorts.FormattingEnabled = true;
            this.CarPorts.Location = new System.Drawing.Point(354, 13);
            this.CarPorts.Name = "CarPorts";
            this.CarPorts.Size = new System.Drawing.Size(90, 21);
            this.CarPorts.TabIndex = 11;
            this.CarPorts.SelectedIndexChanged += new System.EventHandler(this.CarPorts_SelectedIndexChanged);
            // 
            // car_connect_btn
            // 
            this.car_connect_btn.Location = new System.Drawing.Point(354, 41);
            this.car_connect_btn.Name = "car_connect_btn";
            this.car_connect_btn.Size = new System.Drawing.Size(90, 35);
            this.car_connect_btn.TabIndex = 12;
            this.car_connect_btn.Text = "CONNECT CAR";
            this.car_connect_btn.UseVisualStyleBackColor = true;
            this.car_connect_btn.Click += new System.EventHandler(this.car_connect_btn_Click);
            // 
            // SensorsPorts
            // 
            this.SensorsPorts.FormattingEnabled = true;
            this.SensorsPorts.Location = new System.Drawing.Point(464, 12);
            this.SensorsPorts.Name = "SensorsPorts";
            this.SensorsPorts.Size = new System.Drawing.Size(90, 21);
            this.SensorsPorts.TabIndex = 14;
            this.SensorsPorts.SelectedIndexChanged += new System.EventHandler(this.SensorsPorts_SelectedIndexChanged);
            // 
            // sensors_connect_btn
            // 
            this.sensors_connect_btn.Location = new System.Drawing.Point(464, 41);
            this.sensors_connect_btn.Name = "sensors_connect_btn";
            this.sensors_connect_btn.Size = new System.Drawing.Size(90, 35);
            this.sensors_connect_btn.TabIndex = 15;
            this.sensors_connect_btn.Text = "CONNECT SENSORS";
            this.sensors_connect_btn.UseVisualStyleBackColor = true;
            this.sensors_connect_btn.Click += new System.EventHandler(this.sensors_connect_btn_Click);
            // 
            // Car_control
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(578, 415);
            this.Controls.Add(this.sensors_connect_btn);
            this.Controls.Add(this.SensorsPorts);
            this.Controls.Add(this.car_connect_btn);
            this.Controls.Add(this.CarPorts);
            this.Controls.Add(this.log);
            this.Controls.Add(this.center_btn);
            this.Controls.Add(this.right_btn);
            this.Controls.Add(this.left_btn);
            this.Controls.Add(this.stop_btn);
            this.Controls.Add(this.run_btn);
            this.Controls.Add(this.send_btn);
            this.Controls.Add(this.steering);
            this.Controls.Add(this.throttle);
            this.Controls.Add(this.send_line);
            this.Name = "Car_control";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Car control";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.throttle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.steering)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox send_line;
        private System.Windows.Forms.TrackBar throttle;
        private System.Windows.Forms.TrackBar steering;
        private System.Windows.Forms.Button send_btn;
        private System.Windows.Forms.Button run_btn;
        private System.Windows.Forms.Button stop_btn;
        private System.Windows.Forms.Button left_btn;
        private System.Windows.Forms.Button right_btn;
        private System.Windows.Forms.Button center_btn;
        public System.Windows.Forms.TextBox log;
        private System.Windows.Forms.ComboBox CarPorts;
        private System.Windows.Forms.Button car_connect_btn;
        private System.Windows.Forms.ComboBox SensorsPorts;
        private System.Windows.Forms.Button sensors_connect_btn;
    }
}

