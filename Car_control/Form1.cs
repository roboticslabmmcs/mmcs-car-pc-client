﻿using System;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;

namespace Car_control
{
    public partial class Car_control : Form
    {
        public SerialPort car = new SerialPort();
        public SerialPort sensors = new SerialPort();

        delegate void SetTextCallback(string text);

        public string message_car;
        public string message_sensors;
        string[] sensor_data = new string[6];
        string sensor_str = "";
        bool is_auto = false;

        public Car_control()
        {
            InitializeComponent();
        }

        private void SetLogText(string textMessage)
        {
            if (log.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetLogText);
                Invoke(d, new object[] { textMessage });
            }
            else
            {
                if (textMessage == "ASK\r")
                {
                    try
                    {
                        car.WriteLine("ANSWER");
                    }
                    catch
                    {
                    }
                }
                if (textMessage == "OFF\r")
                {
                    is_auto = false;
                }
                log.AppendText(textMessage + "\n");
            }
        }     

        public void BlockAll()
        {
            throttle.Enabled = false;
            steering.Enabled = false;
            right_btn.Enabled = false;
            left_btn.Enabled = false;
            center_btn.Enabled = false;
            stop_btn.Enabled = false;
            send_line.Enabled = false;
            send_btn.Enabled = false;
        }

        public void UnBlockAll()
        {
            throttle.Enabled = true;
            steering.Enabled = true;
            right_btn.Enabled = true;
            left_btn.Enabled = true;
            center_btn.Enabled = true;
            stop_btn.Enabled = true;
            send_line.Enabled = true;
            send_btn.Enabled = true;
        }

        public void OpenConnectionCar()
        {
            if (car.IsOpen)
            {
                car.Close();
                log.AppendText("Closing car port, because it was already open!\n");
            }
            else
            {
                try
                {
                    car.Open();
                    car.ReadTimeout = 1000;
                    log.AppendText("Car Port Opened!\n");
                    car.WriteLine("STATUS");
                }
                catch {}
            }
        }

        public void OpenConnectionSensors()
        {
            if (sensors.IsOpen)
            {
                sensors.Close();
                log.AppendText("Closing sensors port, because it was already open!\n");
            }
            else
            {
                try
                {
                    sensors.Open();
                    sensors.ReadTimeout = 1000;
                    log.AppendText("Sensors Port Opened!\n");
                    sensors.WriteLine("STATUS");
                }
                catch { }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            foreach (string portname in SerialPort.GetPortNames())
            {
                CarPorts.Items.Add(portname);
            }
            if (CarPorts.Items.Count > 0)
            {
                CarPorts.SelectedIndex = 0;
            }

            foreach (string portname in SerialPort.GetPortNames())
            {
                SensorsPorts.Items.Add(portname);
            }
            if (SensorsPorts.Items.Count > 0)
            {
                SensorsPorts.SelectedIndex = 0;
            }

            car.DataReceived += Car_DataReceived;
            sensors.DataReceived += Sensors_DataReceived;
        }

        private void Car_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {                
                string readstring = car.ReadLine();
                SetLogText(readstring);
            }
            catch (TimeoutException)
            {
            }
        }

        private void Sensors_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                string readstring = sensors.ReadLine();
                if (readstring[0] == 'D')
                {
                    readstring = readstring.Substring(1);
                    //string[] commands = readstring.Split('|');
                    //int i = 0;
                    //foreach (string command in commands)
                    //{
                    //    sensor_data[i] = command;
                    //    i++;
                    //}       
                    sensor_str = readstring;
                }
            }
            catch (TimeoutException)
            {
            }
        }

        private void steering_ValueChanged(object sender, EventArgs e)
        {
            if (car.IsOpen)
            {
                car.WriteLine("S" + steering.Value + "A");
            }
        }

        private void throttle_ValueChanged(object sender, EventArgs e)
        {
            if (car.IsOpen)
            {
                car.WriteLine("L" + throttle.Value + "A");
            }
        }

        private void CarPorts_SelectedIndexChanged(object sender, EventArgs e)
        {
            car.Close();
            car.PortName = CarPorts.SelectedItem.ToString();
            car.BaudRate = 19200;
            car.DataBits = 8;
            car.Parity = Parity.None;
            car.StopBits = StopBits.One;
        }

        private void SensorsPorts_SelectedIndexChanged(object sender, EventArgs e)
        {
            sensors.Close();
            sensors.PortName = SensorsPorts.SelectedItem.ToString();
            sensors.BaudRate = 9600;
            sensors.DataBits = 8;
            sensors.Parity = Parity.None;
            sensors.StopBits = StopBits.One;
        }

        private void car_connect_btn_Click(object sender, EventArgs e)
        {
            OpenConnectionCar();
        }

        private void sensors_connect_btn_Click(object sender, EventArgs e)
        {
            OpenConnectionSensors();
        }

        private void send_btn_Click(object sender, EventArgs e)
        {
            if (car.IsOpen)
            {
                car.WriteLine(send_line.Text);
                send_line.Clear();
            }      
        }

        private void stop_btn_Click(object sender, EventArgs e)
        {
            if (car.IsOpen)
            {
                car.WriteLine("HALT");
                throttle.Value = 50;
            }
        }

        private void center_btn_Click(object sender, EventArgs e)
        {
            if (car.IsOpen)
            {
                car.WriteLine("CENTER");
                steering.Value = 50;
            }
        }

        private void right_btn_Click(object sender, EventArgs e)
        {
            if (car.IsOpen)
            {
                car.WriteLine("RIGHT");
            }
        }

        private void left_btn_Click(object sender, EventArgs e)
        {
            if (car.IsOpen)
            {
                car.WriteLine("LEFT");
            }
        }

        private void run_btn_Click(object sender, EventArgs e)
        {
            //car.WriteLine("L60");
            bool was_turn = false;
            is_auto = true;
            while (is_auto)
            {
                string local_data = sensor_str;
                log.AppendText(local_data + "\n");
                try
                {
                    if ((local_data == "00000\r" || local_data == "10001\r") && was_turn == true)
                    {
                        //car.WriteLine("L60");
                        car.WriteLine("CENTER");
                        was_turn = false;
                    }
                    if (local_data == "10000\r" || local_data == "01000\r" || local_data == "01100\r" || local_data == "11000\r" || local_data == "11100\r" || local_data == "00100\r")
                    {
                        //car.WriteLine("L60");
                        car.WriteLine("RIGHT");
                        was_turn = true;
                    }
                    if (local_data == "00001\r" || local_data == "00010\r" || local_data == "00110\r" || local_data == "00011\r" || local_data == "00111\r")
                    {
                        //car.WriteLine("L60");
                        car.WriteLine("LEFT");
                        was_turn = true;
                    }
                    if (local_data == "01110\r")
                    {
                        //car.WriteLine("L40");
                        car.WriteLine("LEFT");
                        was_turn = true;
                    }
                        
                }
                catch
                {
                }
                Thread.Sleep(100);
            }
        }

    }
}
